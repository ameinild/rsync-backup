#!/bin/bash

# Rsync Backup Script
# (Add symlink in /etc/cron.daily)
# Version : 1.0.2-6
# Release : 2021-06-24
# Author  : Artur Meinild

# Initial configuration variables
scriptpath="/path/to/script"

# Include common Bash functions
# shellcheck source=/dev/null
source "${scriptpath}/bash-common.sh"

# Include configuration file
if [[ -f "${scriptpath}/$1" ]]
then
  configfile="${scriptpath}/$1"
else
  configfile="${scriptpath}/rsync-backup.conf"
fi
# shellcheck source=/path/to/script/rsync-backup.conf
source "${configfile}"

# Variable overrides
#sshlist=("host1" "host2" "192.168.7.8")
#homefiles="${scriptpath}/rsync-homefiles-test"
#rootfiles="${scriptpath}/rsync-rootfiles-test"
#varifiles="${scriptpath}/rsync-varifiles-test"

# Local function definition
create_log() {
  if [[ -d "${logfile%/*}" ]]
  then
    [[ -e "${logfile}" ]] && rm "${logfile}"
  else
    logfile="/dev/null"
  fi
}

hosts_online() {
  for i in "${!sshlist[@]}"
  do
    # Ping hosts in "$sshlist[@]", set status to Online/Offline and set full hostname if Online
    if ping -c 3 -W 2 "${sshlist[$i]}" > /dev/null 2>&1
    then
      sshactive[$i]="Online"
      ssh_hname[$i]=$(ping -c 1 "${sshlist[$i]}" | grep '\-\-\-' | awk -F " " '{print $2}')
      [[ "${verbosity,,}" == "yes" ]] && echo "${ssh_hname[$i]} (${sshlist[$i]}) is ${sshactive[$i]}"
    else
      sshactive[$i]="Offline"
      ssh_hname[$i]="Unknown host"
      [[ "${verbosity,,}" == "yes" ]] && echo "${ssh_hname[$i]} (${sshlist[$i]}) is ${sshactive[$i]}"
    fi
  done
}

transform_line() {
  # Set local or remote filetype
  case "${line}" in
  "::LOCAL-ONLY::")
    lore="Local"
    ;;
  "::REMOTE-ONLY::")
    lore="Remote"
    ;;
  "::LOCAL+REMOTE::")
    lore="Local+Remote"
    ;;
  esac
  [[ "${verbosity,,}" == "yes" ]] && echo "Filetype is $lore"

  # Drop lines starting with # or :
  [[ "${line::1}" =~ ^(#|:).* ]] && line=""
}

count_files() {
  # Determine /path/to/file depending on filetype
  case "${filetype}" in
  "Homefiles")
    baseline="/home/${user}/${line}"
    ;;
  "Rootfiles")
    baseline="${line}"
    ;;
  "Varifiles")
    # 1st part of string, use awk
    baseline=$(echo "${line}" | awk -F " " '{print $1}')
    ;;
  esac
  # If directory, count recursively - else increase by 1
  if [[ -d "${baseline}" ]]
  then
    (( count += $(find "${baseline}" -type f | wc -l) ))
  else
    (( count++ ))
  fi
}

print_status() {
  # Add pluralis (file/files)
  if [[ $count -eq 1 ]]
  then
    plu=""
  else
    plu="s"
  fi
  # Print only when line starts with :, and count is not reset
  if [[ "${line::1}" == ":" ]]
  then
    # Print success message if files are synced ( count > 0 ),
    # or failure message when count = 0, then reset counter
    if [[ $count -gt 0 ]]
    then
      echo -e -n " \e[92m\u2713\e[39m  "
      echo -e -n " \u2713  " >> "$logfile"
    elif [[ -n $count ]]
    then
  	  echo -e -n " \e[91m\u2717\e[39m  "
	    echo -e -n " \u2717  " >> "$logfile"
    fi
    # Print message with nr of files and path, when count is not reset
    if [[ -n $count ]]
    then
      printf "%-21s" "$count file${plu} synced" | tee -a "$logfile"
      echo -e "${lore}${locations}" | tee -a "$logfile"
    fi
    locations=""
    count=0
  fi
}

rsync_home_local() {
  for path in "${homepaths[@]}"
  do
    locations="${locations} \u2771 ${path}"
    rsync -za --delete "/home/${user}/${line}" "${path}"
    # shellcheck disable=SC2181
    [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "rsync -za /home/${user}/${line} ${path}"
  done
}

rsync_home_remote() {
  local i=0
  for host in "${sshlist[@]}"
  do
    if [[ "${sshactive[$i]}" == "Online" ]]
    then
      locations="${locations} \u2771 ${user}@${ssh_hname[$i]}"
      rsync -za --delete "/home/${user}/${line}" "${user}@${host}:/home/${user}"
      # shellcheck disable=SC2181
      [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "rsync -za /home/${user}/${line} ${user}@${host}:/home/${user}"
    fi
    (( i++ ))
  done
}

rsync_root_local() {
  for path in "${rootpaths[@]}"
  do
    locations="${locations} \u2771 ${path}"
    rsync -za --delete "${line}" "${path}"
    # shellcheck disable=SC2181
    [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "rsync -za ${line} ${path}"
    if [[ "${chown_r2u,,}" == "yes" ]]
    then
      chown -R "${user}:${user}" "${path}/${line##*/}"
      # shellcheck disable=SC2181
      [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "chown -R ${user}:${user} ${path}/${line##*/}"
    fi
  done
}

rsync_root_remote() {
  local i=0
  for host in "${sshlist[@]}"
  do
    if [[ "${sshactive[$i]}" == "Online" ]]
    then
      locations="${locations} \u2771 root@${ssh_hname[$i]}"
      rsync -za --delete "${line}" "root@${host}:${line%/*}"
      # shellcheck disable=SC2181
      [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "rsync -za ${line} root@${host}:${line%/*}"
    fi
    (( i++ ))
  done
}

rsync_vari_local() {
  file=$(echo "${line}" | awk -F " " '{print $1}')
  path=$(echo "${line}" | awk -F " " '{print $2}')
  ml=$(echo -e "$locations" | grep -c "$path")
  if [[ $ml -eq 0 ]]
  then
    locations="${locations} \u2771 ${path}"
  fi
  rsync -zrlptD --delete "${file}" "${path}"
  # shellcheck disable=SC2181
  [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "rsync -za ${file} ${path}"
}


create_log

script_start >> "$logfile" &
script_start

hosts_online


# Sync homefiles if "$homefiles" exist
if [[ -f "$homefiles" ]]
then
  filetype="Homefiles"
  count=""
  echo " <> Homefiles read from: $homefiles <>" | tee -a "$logfile"

  while read -r line
  do
    # Print status if files are synced
    print_status
    # Default transformation
    transform_line
    if [[ -n "$line" ]]
    then
      locations=""
      # Branch between local and remote files
      case "$lore" in
      "Local")
        rsync_home_local
        ;;
      "Remote")
        rsync_home_remote
        ;;
      "Local+Remote")
        rsync_home_local
        rsync_home_remote
        ;;
      esac
      count_files
    fi
  done < "$homefiles"
fi


# Sync rootfiles if "$rootfiles" exist
if [[ -f "$rootfiles" ]]
then
  filetype="Rootfiles"
  count=""
  echo " <> Rootfiles read from: $rootfiles <>" | tee -a "$logfile"

  while read -r line
  do
    # Print status if files are synced
    print_status
    # Default transformation
    transform_line
    if [[ -n "$line" ]]
    then
      locations=""
      # Branch between local and remote files
      case "$lore" in
      "Local")
        rsync_root_local
        ;;
      "Remote")
        rsync_root_remote
        ;;
      "Local+Remote")
        rsync_root_local
        rsync_root_remote
        ;;
      esac
      count_files
    fi
  done < "$rootfiles"
fi


# Sync varifiles if "$varifiles" exist
if [[ -f "$varifiles" ]]
then
  filetype="Varifiles"
  count=""
  echo " <> Varifiles read from: $varifiles <>" | tee -a "$logfile"

  while read -r line
  do
    # Print status if files are synced
    print_status
    # Default transformation
    transform_line
    if [[ -n "$line" ]]
    then
      # Branch between local and remote files
      case "$lore" in
      "Local")
        rsync_vari_local
        ;;
      "Remote")
        rsync_vari_local
        ;;
      esac
      count_files
    fi
  done < "$varifiles"
fi


script_end "nocount/noexec/nomail" >> "$logfile" &
script_end "nocount/noexec/nomail"
