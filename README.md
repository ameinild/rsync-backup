# BASH Script: rsync-backup

This BASH script can perform backup of homefiles, rootfiles and various other files (varifiles) to predefined local and remote locations using `rsync`.

The script lets you define and customize the following functionality:

* Define any number of homefiles (files/directories in /home/$user) that are synced to predefined locations - either locally, remotely or both
* Define any number of rootfiles (files/directories anywhere on your machine) that are synced to predefined locations - either locally, remotely or both
* Define any number of varifiles (arbitrarily defined files/directories) where source and destination are set manually (can be both local and remote)

The script is intended for running daily with cron to automate the above mentioned tasks.

The script validates with `shellcheck -x`.

## Preliminary information

* This script must be run as a root user
* This script requires the support script `bash-common.sh` to be placed (or linked) in the same directory as the script
* This script requires that you have installed private and public SSH keys if you want remote connection to work

## The configuration file

This script comes with a default configuration file: `rsync-backup.conf`

The configuration file allows for the following general options:

* `logfile=` : Sets the file where formatted output is logged (if set to an invalid path, logfile is set to /dev/null)
* `user=` : Sets the default user (used mainly for homefiles)
* `chown_r2u=` : Yes/No. Indicates if synced rootfiles should have owner changed to the above user (local locations only)
* `verbosity=` : Yes/No. Indicates if the script echoes the commands in addition to performing them

In addition, the configuration file contains the following 3 arrays, defining paths for homefiles/rootfiles and listing remote hosts:

* `homepaths=` : Array with a single path string for each destination directory for local homefile backup
* `rootpaths=` : Array with a single path string for each destination directory for local homefile backup
* `sshlist=` : Array with a single path string for each hostname or IP address of remote hosts (where homefiles and rootfiles will be synced remotely)

The default configuration file contains examples of these arrays.

Finally, the configuration file points to 3 definition list files, which are explained in the next section:

* `homefiles` : Points to the file that includes a list of homefiles to be synced
* `rootfiles` : Points to the file that includes a list of rootfiles to be synced
* `varifiles` : Points to the file that includes a list of varifiles to be synced

## The definitions list files

The configuration file must point to up to 3 definition list files: `homefiles`, `rootfiles` and `varifiles` (if an invalid file is referenced, it is simply skipped).

Each definition list file is constructed in the same manner, and holds information about which files in each category that are copied when the script is run.

Each definition list file has 3 blocks:

* `::LOCAL-ONLY::` : Files in this section will be synced only locally (using `homepaths` or `rootpaths`)
* `::REMOTE-ONLY::` : Files in this section will be synced only remotely (to the original location, using `sshlist`)
* `::LOCAL+REMOTE::` : Files in this section will be synced both locally and remotely (using the above)

Each file ends with the code `::END::`, which tells the script that there are no more blocks to process.

Please note that the definition list file `varifiles` work a little different than the other 2. This file manually sets both source and destination one at a time, so neither `homepaths`, `rootpaths` or `sshlist` is used here - everything is defined manually.

Also note that `varifiles` only has `::LOCAL-ONLY::` and `::REMOTE-ONLY::` - and these blocks actually does the same thing (because of the manual configuration mentioned above), so this is basically only for flavor.

The 3 included definition list files (`rsync-homefiles`, `rsync-rootfiles` and `rsync-varifiles`) gives examples of their usage.

## The script file

The main script file `rsync-backup.sh` also needs to be adjusted a bit before use.

* Line 10: Set the path where the script and configuration files are executed from
* Line 23: Set the same path (for shellcheck validation only)
* Line 27-30: Can be used to override `sshlist` and definition list files (`homefiles`, `rootfiles` and `varifiles`) while testing

## Running the script

When all parameters are set correctly, the script can be run with `./rsync-backup.sh`.

The recommended method of execution is putting the script in your chosen destination, and making a symlink in `/etc/cron.daily` to the script. Note that the symlink should be named `rsync-backup` without extension.
