#!/bin/bash

# Bash Common Functions
# Version : 1.1.0-10
# Release : 2021-06-24
# Author  : Artur Meinild

# Initial configuration variables
efromadd="user@${HOSTNAME}.server.fqdn"
emailadd="yourmail@provider.com"
sendpath="/usr/sbin/sendmail"

nr_success=0
nr_failure=0
reboot="No"
[[ -f /var/run/reboot-required.pkgs ]] && reboot="Yes"


# Timestamp function
timestamp() {
  ts=$(date '+%Y-%m-%d %H:%M:%S')
  echo -e " \u23F1  $ts"
}


# Reformatted uptime function
uptime2() {
  newup=$(uptime | awk -F '( |,|:)+' '{
    d=h=m=0;
    if ($7=="min")
      m=$6;
    else {
      if ($7~/^day/) {
        d=$6;
        h=$8;
        m=$9
      }
      else {
        h=$6;
        m=$7
      }
    }
  }
  {
    print d+0,"days,",h+0,"hours,",m+0,"minutes"
  }')

  echo "$(echo "$newup" | cut -d ' ' -f1)d $(echo "$newup" | cut -d ' ' -f3)h $(echo "$newup" | cut -d ' ' -f5)m"
}


# Function to show spinner while running command in the background
show_spinner() {
  if [[ -z $1 ]]
  then
    PID=$!
  else
    PID=$1
  fi
  ii=0
  sp="\|/-"
#  echo -n "    "
  while [[ -d "/proc/$PID" ]]
  do
    ch="${sp:ii++%${#sp}:1}"
    printf "%b" " [${ch}]"
    sleep .1
    printf "\b\b\b\b"
  done
}


# Function to show progbar while running command in the background
# "improve redraw after break"
show_progbar() {
  if [[ -z $1 ]]
  then
    PID=$!
  else
    PID=$1
  fi
  ii=0
  jj=0
  bar="="
  echo -n " [ "
  while [[ -d "/proc/$PID" ]]
  do
    printf "%b" "\b${bar}]"
    (( ii++ ))
    sleep .1
  done
  (( ii=ii+3 ))
  while [[ $jj -lt $ii ]]
  do
    printf "\b"
    (( jj++ ))
  done
}


# Function to show spinbar while running command in the background
# "improve redraw after break"
show_spinbar() {
  if [[ -z $1 ]]
  then
    PID=$!
  else
    PID=$1
  fi
  ii=0
  jj=0
  kk=0
  sp="\|/-"
  bar="="
  echo -n " [  "
  while [[ -d "/proc/$PID" ]]
  do
    ch="${sp:ii++%${#sp}:1}"
    printf "%b" "\b\b${ch}]"
    sleep .1
    if [[ $ch == "-" ]]
    then
      printf "%b" "\b\b${bar}] "
      (( jj++ ))
    fi
  done
  (( jj=ii+3 ))
  while [[ $kk -lt $jj ]]
  do
    printf "\b"
    (( kk++ ))
  done
}


# Function to show counter while running command in the background
show_counter() {
  if [[ -z $1 ]]
  then
    PID=$!
  else
    PID=$1
  fi
  [[ ! -v ii ]] && ii=0
  while [[ -d "/proc/$PID" ]]
  do
    kk=0
    printf "%b" " [${ii}]"
    (( ii++ ))
    jj=${#ii}
    sleep 1
    printf "\b\b\b"
    while [[ $kk -lt $jj ]]
    do
      printf "\b"
      (( kk++ ))
    done
  done
}


# Function to count how many times a script has run (cron stats)
script_count() {
  touch "$0".nr
  count=$(( $(< "$0".nr) + 1 ))
  echo "$count" > "$0".nr
}


# Script Start - prints a starting message with script name and intepreter
script_start() {
  log_error
  echo -e " \u00A0"
#  startime=$(date '+%Y-%m-%d %H:%M:%S')
  timestamp
  echo "Starting script: $0"
  echo "Interpreter: $(head -n1 "$0")"
  echo " [>"
}


# Script Source - prints a message when sourcing another script
# Usage: script_source "/path/to/script"
script_source() {
  timestamp
  echo "Sourcing script: $1"
  echo "Interpreter: $(head -n1 "$1")"
  echo " =>"
  # shellcheck source=/dev/null
  source "$1"
}


# Script Inter - prints an intermediate separator to divide commands into blocks
script_inter() {
  echo " <>"
}


# Script End - prints an ending message with script name and custom summary (count, exec, boot status)
# Also sends email if "nomail" is not included
# Usage: script_end "nocount/noexec/noboot/nomail"
script_end() {
  # If any commands ended with failure, keep errorlog - otherwise remove it
  [[ "$nr_failure" -eq 0 ]] && [[ "$errorlog" != "/dev/null" ]] && rm "$errorlog"

  echo " <]"
#  stoptime=$(date '+%Y-%m-%d %H:%M:%S')
  timestamp
  echo "Ending script: $0"
  echo "Summary:"
  echo -ne " \u270E  "
  [[ $1 != *"nocount"* ]] && echo -ne "Script has run: $count times\n    "
  [[ $1 != *"noexec"* ]] && echo -ne "${nr_success} command(s) executed with success\n    "
  [[ $1 != *"noexec"* ]] && echo -ne "${nr_failure} command(s) executed with failure\n    "
  [[ $1 != *"noboot"* ]] && echo -ne "Computer uptime: $(uptime2)\n    "
  [[ $1 != *"noboot"* ]] && echo -ne "Reboot required: ${reboot}\n    "
  echo " "

  [[ $1 != *"nomail"* ]] && exit_email
}


# File transformation function
# Usage: file_transform "sourcefile" "targetfile" "sacredfile"
# The helper function 'transform_command' must be defined locally,
# containing the operation to perform on each input "$line"
file_transform() {
  timestamp
  [[ -z "$line" ]] && line="line-from-file"

  echo "Transforming from: $line to:"
  transform_command

  [[ -f "$2" ]] && rm "$2"
  touch "$2"

  # Read every line in "sacredfile" (if it is defined), and create an array
  if [[ -n "$3" ]]
  then
    iarray=0

    while read -r line
    do
      sarray[$iarray]="$line"
      (( iarray++ ))
    done < "$3"
  else
    sarray[0]=""
  fi

  # Read every line in "sourcefile", exclude "sacredfile" entries,
  # perform 'transform_command' and write output to "targetfile"
  while read -r line
  do
    smatch=0

    # Iterate over exclude array
    for val in "${sarray[@]}"
    do
      [[ "$val" == "$line" ]] && (( smatch++ ))
    done

    # echo $smatch
    [[ $smatch -eq 0 ]] && transform_command >> "$2"
  done < "$1"

  sourcesize=$(wc -l < "$1")
  echo "Successfully transformed $sourcesize entries!"
}


# Runs a command with parameters, and displays exit status
# Usage: exec_command "command" "parameters" "options"
# Command and parameters can be combined, as long as they are in quotes
exec_command() {
  timestamp >> >(tee -a "$errorlog"); sleep 0.001
  run_status "$1" "$2" "$3" >> >(tee -a "$errorlog"); sleep 0.001
  if [[ -z "$3" ]]
  then
    # shellcheck disable=SC2086
    $1 $2 >> >(tee -a "$errorlog") 2>> >(tee -a "$errorlog" >&2)
  else
    # shellcheck disable=SC2086
    $1 $2 $3 >> >(tee -a "$errorlog") 2>> >(tee -a "$errorlog" >&2)
  fi
  exit_status "$1" >> >(tee -a "$errorlog"); sleep 0.001
  echo "" >> "$errorlog"; sleep 0.001
}


# Checks if current command is run with parameters, and displays command (plus parameters)
run_status() {
  if [[ -n "$2" ]]
  then
    echo "Running: \"$1\" with parameters: $2"
    [[ -n "$3" ]] && echo "Options: $3"
  else
    echo "Running: \"$1\" with parameters: none"
    [[ -n "$3" ]] && echo "Options: $3"
  fi
}


# Checks exit status of the last executed command, and displays success or failure message
exit_status() {
  last_exit=$?
  if [[ $last_exit -eq 0 ]]
  then
    echo "Success! \"$1\" exited with error code $last_exit"
    (( nr_success++ ))
  else
    echo "Failure! \"$1\" exited with error code $last_exit"
    (( nr_failure++ ))
  fi
}


# Exit email function
exit_email() {
#  (( scr_seconds = $(date -d "$stoptime" +%s) - $(date -d "$startime" +%s) ))

  e_message="Script $0 has completed its execution in $SECONDS seconds."

  if (( nr_failure == 0 ))
  then
#    echo "Status: Script $0 completed with full success"
    e_subject="Status: Script $0 completed with full success"
    (( nr_success != 0 )) && e_message="${e_message}\n\n${nr_success} commands executed with success."
  else
    if (( nr_success == 0 ))
    then
#      echo "Error: Script $0 completed, but with errors"
      e_subject="Error: Script $0 completed, but with errors"
      e_message="${e_message}\n\n${nr_failure} commands executed with failure."
    else
#      echo "Notice: Script $0 completed with partial success"
      e_subject="Notice: Script $0 completed with partial success"
      e_message="${e_message}\n\n${nr_success} commands executed with success."
      e_message="${e_message}\n${nr_failure} commands executed with failure."
    fi
  fi

  # shellcheck disable=SC2154
  [[ -f "$logfile" ]] && e_message="${e_message}\n\nSee attached logfile (${logfile}) for details."

  send_email
}

# Send email function
# Usage: send_email
# This command sends an email with status of the currently executed script.
# Required variables are:
#   - "$efromadd"  :  Email address of receiver
#   - "$emailadd"  :  Email address of receiver
#   - "$sendpath"  :  Path of sendmail program
#   - "$e_subject" :  Subject of email (head)
#   - "$e_message" :  Message of email (body)
#   - "$logfile"   :  Name of attached logfile (if any)
send_email() {
  if [[ -f "$logfile" ]]
  then
    sendemail -f "$efromadd" -t "$emailadd" -S "$sendpath" -u "$e_subject" -m "$e_message" -a "$logfile" -q
  else
    sendemail -f "$efromadd" -t "$emailadd" -S "$sendpath" -u "$e_subject" -m "$e_message" -q
  fi
}


# Error log function
log_error() {
  errorfile=$(echo "$0" | awk -F "/" '{print $NF}')

  # If $errorpath is empty, set errorlog="/dev/null" - otherwise set proper name
  # shellcheck disable=SC2154
  if [[ -z "${errorpath}" ]]
  then
    errorlog="/dev/null"
  else
    errorlog="${errorpath}/${errorfile}.err.log"
  fi
  echo -e " \u00A0" >> "$errorlog"; sleep 0.001
  timestamp >> "$errorlog"; sleep 0.001
  echo "==ERROR LOG== for script: $0" >> "$errorlog"; sleep 0.001
  echo "" >> "$errorlog"; sleep 0.001
}


# Rotate log function
# Usage: ./bash_common.sh --log-rotate /path/to/logfiles <number-of-days>D OR <number-of-lines>L
# This command rotates the following 3 levels of logfiles:
#   - "current log"  :  script.sh.log
#   - "previous log" :  script.sh.log.1.gz
#   - "archived log" :  script.sh.log.2.gz
log_rotate() {
  ii=0

  # Create named pipe
  [[ ! -d  ~/pipe ]] && mkdir ~/pipe
  [[ ! -e  ~/pipe/log-list ]] && mkfifo ~/pipe/log-list

  # List logfiles in dir and write them to pipe
  touch ~/pipe/log-list
  ls -1 "$2"/*.log > ~/pipe/log-list &

  # Read every line (logfile) and do log archive routine
  while read -r line
  do
    echo "Processing ${line}"

    # Determine logtype
    datefile="$2/datefile"
    case "$3" in
      *"D")
        num="${3::-1}"
        logtype="Timelog"
        ;;
      *"L")
        num="${3::-1}"
        logtype="Linelog"
        ;;
      *)
        num=0
        logtype="Timelog"
        ;;
    esac

    # Run rotate function depending on logtype
    case "$logtype" in
      "Timelog")
        (( datediff=$(date '+%s')-$(( 86400*num )) ))
        pastdate=$(date -d "@${datediff}")
        touch -d "${pastdate}" "${datefile}"

        # Rotate only if logfile is more than #number-of-days old (0 rotates always)
        if [[ "${line}" -ot "${datefile}" ]]
        then
          # Check if "archived log" is present (unzip and move)
          if [[ -f "${line}.2.gz" ]]
          then
            gunzip "${line}.2.gz"
            mv "${line}.2" "${line}.2.old"
          fi
          # Check if "previous log" is present (unzip, move and zip)
          if [[ -f "${line}.1.gz" ]]
          then
            gunzip "${line}.1.gz"
            mv "${line}.1" "${line}.2"
            if [[ -f "${line}.2.old" ]]
            then
              cat "${line}.2.old" >> "${line}.2"
              rm "${line}.2.old"
            fi
            gzip "${line}.2"
          fi
          # Check if "current log" is present (move and zip)
          if [[ -f "${line}" ]]
          then
            mv "${line}" "${line}.1"
            gzip "${line}.1"
          fi
        fi
        ;;
      "Linelog")
        touch -r "${line}" "${datefile}"
        tail -n "${num}" "${line}" > "${line}.new"
        cp "${line}.new" "${line}"
        rm "${line}.new"
        touch -r "${datefile}" "${line}"
        ;;
      *)
        echo "No logtype defined"
        ;;
    esac

    (( ii++ ))
    [[ -f "${datefile}" ]] && rm "${datefile}"
  done < ~/pipe/log-list

  echo "${ii} log(s) rotated"
}


# Run rotate log function if parameter is --log-rotate
if [[ "$1" == "--log-rotate" ]] && [[ -d "$2" ]]
then
  log_rotate "$1" "$2" "$3"
fi
